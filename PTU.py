from time import sleep
import serial
import socket

# sudo chmod 666 /dev/ttyUSB0 enter for serial communication
class PTU():
    def __init__(self, serial_port = "/dev/ttyUSB0"):
        self.serial_port = serial_port
        self.ser = serial.Serial(self.serial_port)
        print("Serial communication started over {}".format(self.serial_port))
        
        self.sock = None
        self.IP = None
        self.port = None
        
        self.step_mode = None
        self.resolution = None
        
        # command execution
        self.total_fail = 0
        self.execution_state = True
        self.first_failure = True

    def serial_send(self, command):
        self.ser.write(("{} ".format(command)).encode("utf-8"))
        sleep(0.02) # wait for 20ms for the PTU response
        received_decoded = None
        while self.ser.inWaiting():
            received_encoded = self.ser.readline()
            received_decoded = received_encoded.decode("utf-8")
        return received_decoded

    def serial_close(self):
        self.ser.close()
        print("serial communication over {} has been closed".format(self.serial_port))
        self.serial_port = None
        self.ser = None
    
    def success(self, r):
        return r if((r != None) and (r.find('*') != -1) and (r.find('!') == -1)) else None
    
    def get_IP(self):
        r = self.serial_send("NI")
        if self.success(r) != None:
            start = r.find("IP: ") + len("IP: ")
            end = r.find("\r\n")
            return r[start: end]
        else:
            return None
        
    def start_socket(self):
        print("Starting the socket..")
        if ((self.sock == None) and (self.IP == None) and (self.port == None)):
            IP = self.get_IP()
            if IP != None:
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.port = 4000
                self.IP = IP
                self.sock.connect((self.IP, self.port))
                sleep(0.1)
                print("Socket has been started over {}:{}".format(self.IP, self.port))
                print(self.sock.recv(2048).decode("utf-8"))
            else:
                print("Serial respond from the PTU was very slow! Please configure the sleep-time after writing to the serial port.")
        else:
            print("Socket has been already started over 0{}:{}".format(self.IP, self.port))
    
    def socket_close(self):
        self.sock.close()
        self.sock = None
        
    def socket_send(self, command):
        self.sock.send(("{} ".format(command)).encode("utf-8"))
        sleep(0.001)
        received = self.sock.recv(2048).decode("utf-8")
        # return the received command if the command or the query sent was succesfully executed otherwise return None
        return self.success(received)
    
    def sock_receive(self):
        return self.sock.recv(2048).decode("utf-8")

    def execute_command(self, command):
        received = self.socket_send(command)
        if received != None:
            self.execution_state = True
            print(received)
        else:
            self.execution_state = False
            print("Command execution Failed!")
            if self.first_failure != self.execution_state:
                print("Execution Failed for the first time")
                print(self.sock.recv(2048).decode("utf-8"))
            else:
                print("Execution failed more than once")
        
            self.first_failure = self.execution_state

        print(self.execution_state)
        print(self.first_failure)

    def set_step_mode(self, step_mode):
        step_modes = {
            "half": ["WPH", "WTH", 0.02],
            "quarter": ["WPQ", "WTQ", 0.01],
            "eighth": ["WPE", "WTE", 0.005],
        }
        self.execute_command(step_modes.get(step_mode)[0])
        print("sleeping for 0.5 second")
        sleep(0.5)
        self.execute_command(step_modes.get(step_mode)[1])
        print("sleeping for 0.5 second")
        sleep(0.5)
        self.axis_reset(pan = True, tilt = True)
        self.resolution = step_modes.get(step_mode)[2]
    
    def axis_reset(self, pan = False, tilt = False):
        if pan:
            self.socket_send("RP")
            print("sleeping for 2 seconds")
            sleep(2)
        if tilt:
            self.socket_send("RT")
            print("sleeping for 2 seconds")
            sleep(2)
        else:
            print("Please specify at least on axis, pan or tilt for axis reseting!")

    def move_x_to(self, position):
        # example respond: "PP100 *"
        command = "PP{}".format(position)
        self.execute_command(command)

    def move_y_to(self, position):
        # example respond: "TP100 *"
        command = "TP{}".format(position)
        self.execute_command(command)

    def move_x_by(self, num_of_positions):
        # example respond: "PO100 *"
        command = "PO{}".format(num_of_positions)
        self.execute_command(command)

    def move_y_by(self, num_of_positions):
        # example respond: "TO100 *"
        command = "TO{}".format(num_of_positions)
        self.execute_command(command)

    def num_of_positions(self, angle):
        return int(angle/self.resolution)
    
    def move_x_to_degrees(self, angle):
        position = self.num_of_positions(angle)
        self.move_x_to(str(position))
    
    def move_y_to_degrees(self, angle):
        position = self.num_of_positions(angle)
        self.move_y_to(str(position))

    def move_x_by_degrees(self, angle):
        num_of_positions = self.num_of_positions(angle)
        self.move_x_by(str(num_of_positions))
    
    def move_y_by_degrees(self, angle):
        num_of_positions = self.num_of_positions(angle)
        self.move_y_by(str(num_of_positions))
    
    def ptu_await(self):
        # PTU waits to complete the previous position commands
        command = "A"
        self.execute_command(command)
    
    '''
    def execute_query(self, query):
        received = self.socket_send(query)
        print(received)
        if received != None:
              
        else:
            print("Query execution failed!")

    def get_x_position(self):
        #PP * Current Pan position is 0
        query = "PP"
        return self.socket_send(query)
    
    def get_y_position(self):
        query = "TP"
        return self.socket_send(query)
    '''