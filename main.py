import cv2
import numpy as np
import dlib
from argparse import ArgumentParser
from os import sys
from Tracker import Tracker
from PTU import PTU
from PID import PID
import time

def valid_tracker(tracker_name):
    tracker_names = ["csrt","kcf","mil", "correlationTracker"]
    if tracker_name in tracker_names:
        return tracker_name
    else:
        print("{} tracker is not avaiable! Please use of those: ")
        for idx, tracker in enumerate(tracker_names):
            print("{}. {}".format(idx, tracker))
        return None

def main():
    parser = ArgumentParser()
    parser.add_argument("-v", "--video", required=True, help="video path, to find out the video camera path use 'ls /dev/video*'")
    parser.add_argument("-t", "--tracker", required=True, help='Tracker algorithm. Available tracking algorithms: ["csrt","kcf","mil", "correlationTracker"]')
    parser.add_argument("-s", "--serial", required=False, help="Serial port to communicate with the PTU. to find out write 'ls /dev/tty*' on terminal")

    args = vars(parser.parse_args())

    tracker = None
    initial_bounding_box = None

    tracker_name = valid_tracker(args["tracker"])
    if tracker_name is None:
        sys.exit("Exiting the program.")
    
    if args["serial"] != None:
        # kP kI kD
        x_PID = [0.02, 0, 0]
        y_PID = [0.02, 0, 0]
        prev_error_x = 0
        prev_error_y = 0
        integral_x = 0
        integral_y = 0
        ptu = PTU(args["serial"])
        ptu.start_socket()
        ptu.serial_close()
        ptu.set_step_mode("eighth")
        ptu.move_x_to_degrees(0)
        ptu.move_y_to_degrees(0)
    else:
        print("You did not choose to activate the PTU!")
        
    tracker = Tracker(tracker_name)
    tracker.initialize_tracker()

    video_capture = cv2.VideoCapture(args["video"])

    while(True):
        ret, frame = video_capture.read()

        if ret is False:
            print("Could not read a frame over {}".format(args["video"]))
            break
        
        # if the user set the bounding box or object detection happened
        if (initial_bounding_box is not None) and (tracker is not None):
            # update bounding box object [x, y, w, h] by updating the tracker
            if tracker.tracker_type == "opencvTracker":
                tracker.update_bounding_box(frame)
            elif tracker.tracker_type == "correlationTracker":
                rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                tracker.update_bounding_box(rgb)
            else:
                print("tracker type is not valid!")
            # update the bounding box location on the current frame
            tracker.update_object_center()
            
            # red circle on the center of the current frame
            (H, W) = frame.shape[:2]
            frame_center_x = W // 2
            frame_center_y = H // 2
            cv2.circle(frame, (frame_center_x, frame_center_y), 3, (0,0,255), 3)
            
            last_oc = tracker.get_last_object_center()
            last_bb = tracker.get_last_bounding_box()

            # check if the object is lost
            if tracker.lost:
                print("object is lost - last center: {}, {} - last bounding box(x, y, w, h): {}, {}, {}, {}".format(last_oc[0], last_oc[1], last_bb[0], last_bb[1], last_bb[2], last_bb[3]))
            if (not tracker.lost) and (len(last_oc) > 0) and (len(last_bb) > 0):
                print("tracking the object - current center: {}, {} - current bounding box(x, y, w, h): {}, {}, {}, {}".format(last_oc[0], last_oc[1], last_bb[0], last_bb[1], last_bb[2], last_bb[3]))
                # green circle on the center of the object
                cv2.circle(frame, (last_oc[0], last_oc[1]), 3, (0, 255, 0), 3)
                # green rectangle around the object that is being tracked
                cv2.rectangle(frame, (last_bb[0], last_bb[1]), (last_bb[0] + last_bb[2], last_bb[1] + last_bb[3]), (0, 255, 0), 2)

            if args["serial"] != None:
                # distance(aka. error) between frame_center and object_center
                error_x = last_oc[0] - frame_center_x

                error_y = frame_center_y - last_oc[1]

                print("Error in x: {}, in y: {}".format(error_x, error_y))

                # PID for x
                proportional_x = error_x
                integral_x = integral_x + error_x
                differential_x = abs(prev_error_x - error_x)
                u_x = round(x_PID[0] * proportional_x + x_PID[1] * integral_x + x_PID[2] * differential_x, 3)

                # PID for y
                proportional_y = error_y
                integral_y = integral_y + error_y
                differential_y = abs(prev_error_y - error_y)
                u_y = round(x_PID[0] * proportional_y + x_PID[1] * integral_y + x_PID[2] * differential_y, 3)

                prev_error_x = error_x
                prev_error_y = error_y

                print("u_x(t): {}, u_y(t): {}".format(u_x, u_y))
                #u_x = int(u_x * 1800)
                if error_x**2 > 100:
                    ptu.move_x_by_degrees(u_x)
            
                if error_y**2 > 100:
                    ptu.move_y_by_degrees(-u_y)

        cv2.imshow("Frame", frame)

        # get the user input
        key = cv2.waitKey(1) & 0xFF

        if(key == ord("s")):
            initial_bounding_box = cv2.selectROI("Frame", frame, fromCenter = False, showCrosshair=True)
            if tracker.tracker_type == "opencvTracker":
                tracker.start_tracker(initial_bounding_box, frame)
            elif tracker.tracker_type == "correlationTracker":
                rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                tracker.start_tracker(initial_bounding_box, rgb)
            else:
                print("could not start the tracker!")
        
        if(key == ord("q")):
            video_capture.release()
            cv2.destroyAllWindows()

            if args["serial"] != None:
                ptu.move_x_to(0)
                ptu.move_y_to(0)
                ptu.socket_close()

            sys.exit("Exiting the program.")

if __name__ == "__main__":
    main()