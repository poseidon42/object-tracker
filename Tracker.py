import cv2
import dlib

class Tracker:
    def __init__(self, tracker_name):
        self.tracker_name = tracker_name
        self.tracker_type = self.tracker_type()
        self.bounding_box = None
        self.object_center = None
        self.lost = None

    def tracker_type(self):
        if self.tracker_name in ["csrt","kcf","mil"]:
            return "opencvTracker"
        elif self.tracker_name == "correlationTracker":
            return "correlationTracker"
        else:
            return None

    def initialize_tracker(self):
        object_trackers = {
            "csrt": cv2.TrackerCSRT_create,
		    "kcf": cv2.TrackerKCF_create,
		    "mil": cv2.TrackerMIL_create,
            "correlationTracker": dlib.correlation_tracker
        }
        self.tracker = object_trackers[self.tracker_name]()
    
    def start_tracker(self, initial_bounding_box, frame_or_rgb):
        self.lost = False
        if self.tracker_type == "opencvTracker":
            self.tracker.init(frame_or_rgb, initial_bounding_box)
            print("{} tracker is started.".format(self.tracker_name))
        elif self.tracker_type == "correlationTracker":
            rect = dlib.rectangle(initial_bounding_box[0], initial_bounding_box[1], initial_bounding_box[0] + initial_bounding_box[2], initial_bounding_box[1] + initial_bounding_box[3])
            self.tracker.start_track(frame_or_rgb, rect)
            print("{} tracker is started.".format(self.tracker_name))
        else:
            print("The tracker algorithm is not valid")

    def update_bounding_box(self, frame_or_rgb):
        if self.tracker_type == "opencvTracker":
            success, bounding_box = self.tracker.update(frame_or_rgb)
            if success:
                self.bounding_box = bounding_box
                self.lost = False
            elif not success:
                self.lost = True
                print("Object is lost!")
            else:
                print("Something unexpected occured while updating the bounding box of the object that is being tracked!!!")
        elif self.tracker_type == "correlationTracker":
            self.tracker.update(frame_or_rgb)
            pos = self.tracker.get_position()
            startX = int(pos.left())
            startY = int(pos.top())
            endX = int(pos.right())
            endY = int(pos.bottom())
            
            self.bounding_box = startX, startY, endX - startX, endY - startY
            print("bounding box is updated")
        else:
            print("The tracker algorithm is not valid")

    def update_object_center(self):
        try:
            x, y, w, h = [int(a) for a in self.bounding_box]
            object_center_x = int(x + (w / 2.0))
            object_center_y = int(y + (h / 2.0))

            self.object_center = [object_center_x, object_center_y]
        except:
            print("Failed to update the center of the object!")
            self.bounding_box_available = False
    
    def get_last_object_center(self):
        return self.object_center
    
    def get_last_bounding_box(self):
        return self.bounding_box