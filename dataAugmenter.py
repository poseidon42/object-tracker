from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
import numpy as np
import argparse
import os
from argparse import ArgumentParser
import sys

def get_image_locations(input_images_dir):
    images = []
    for filename in os.listdir(input_images_dir):
        img = load_img(os.path.join(input_images_dir, filename))
        if img is not None:
            images.append(os.path.join(input_images_dir, filename))
        
    return images

def generate_images(output_images_dir, img_path, num_of_generations):
    img = load_img(img_path)
    img_arr = img_to_array(img)
    img_samples = np.expand_dims(img_arr, axis = 0)

    # data_generator object randomly rotates, zooms, shifts, shears and flips the original image to create several copies
    data_generator = ImageDataGenerator(
    rotation_range=30,
    zoom_range=0.15,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.15,
    horizontal_flip=True,
    fill_mode="nearest")

    _, img_name = os.path.split(img_path)

    # image_generator will randomly transform the input image
    image_generator = data_generator.flow(img_samples, batch_size=1, save_to_dir="{}".format(output_images_dir), save_prefix=img_name, save_format="jpg")

    for _ in image_generator:
        num_of_generations -= 1
        if num_of_generations <= 0:
            break

def convertable(x):
    try:
        int(x)
        return True
    except ValueError:
        return False

def main():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required=True, help="Input directory where the images to be augmented locate.")
    parser.add_argument("-o", "--output", required=True, help="Output directory where the augmented images to write in.")
    parser.add_argument("-n", "--number", required= True, help="Number of images to augment each image.")
    args = vars(parser.parse_args())

    if not convertable(args["number"]):
        sys.exit("--number must be an integer")
    try:
        for image_location in get_image_locations(args["input"]):
            generate_images(args["output"], image_location, int(args["number"]))

        print("{} image(s) for each image in the {} directory was succesfully augmented into {} directory".format(args["number"], args["input"], args["output"]))
    except:
        e = sys.exc_info()[0]
        print("Something went wrong!\n{}".format(e))

if __name__ == "__main__":
    main()