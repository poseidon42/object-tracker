from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import re
import urllib.request
import os
from argparse import ArgumentParser
import sys

def find_images(lst):
    links = []
    for x in lst:
        if ("Image result for" in x) and ("src" in x):
            start_index = x.find("src") + len('src="')
            x = x[start_index:]
            last_index = x.find('"')
            x = x[0:last_index]
            links.append(x)
    return links

def install_images(links, folder_name = "images", keyword = "image"):
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
    for idx, link in enumerate(links):
        img_name = "{}/{}{}.jpg".format(folder_name, keyword, idx)
        urllib.request.urlretrieve(link, img_name)

def convertable(x):
    try:
        int(x)
        return True
    except ValueError:
        return False

def main():
    parser = ArgumentParser()
    parser.add_argument("-k", "--keyword", required=True, help="Keyword to search for images")
    parser.add_argument("-o", "--output", required=True, help="Path to output the images")
    parser.add_argument("-l", "--level", required= True, help="level increases proportional to num. of images, ranges from 1 to 5")
    args = vars(parser.parse_args())

    if not convertable(args["level"]):
        sys.exit("--level must be integer between 1 and 5")

    try:
        options = Options()
        options.headless = True
        browser = webdriver.Firefox(options=options)
        browser.get('https://www.google.de/imghp?hl=en&ogbl')

        # get the element to search
        search_element = browser.find_element_by_name("q")
        search_element.send_keys(args["keyword"]) # fill the search
        search_element.send_keys(Keys.RETURN)

        # move the page down to load more images
        counter = int(args["level"]) - 1
        for _ in range(counter):
            browser.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(3)
        
        ps = browser.page_source

        lst = ps.split("<img class=")

        links = find_images(lst)

        install_images(links, args["output"], args["keyword"])
        print("Successfully installed {} images, check {}".format(args["keyword"], args["output"]))

    finally:
        try:
            browser.quit()
            print("Succesfully closed the browser")
        except:
            print("Something went wrong")

if __name__ == "__main__":
    main()